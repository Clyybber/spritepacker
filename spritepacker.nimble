# Package
version       = "0.1.0"
author        = "Clyybber"
description   = "A 2D texture packer"
license       = "MIT"

# Structure
srcDir = "src"
binDir = "bin"
bin    = @["spritepacker"]

# Dependencies
requires "stb_image"
requires "cligen"
