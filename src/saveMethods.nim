from spritepacker import sprite

type saveMethod* = enum
  nimTuple
  commaSepValues

func nimTupleSav*(paths: seq[string], sprites: seq[sprite]): string =
  result.add "const data = ("
  for sprite in sprites:
    result.add paths[sprite.textureIndex] & ": (x: " & $sprite.x & ", y: " & $sprite.y & ", width: " & $sprite.width & ", height: " & $sprite.height & ", rotated: " & $sprite.rotated & "), "
  result.add ")"

func commaSepValuesSav*(paths: seq[string], sprites: seq[sprite]): string =
  for sprite in sprites:
    result.add paths[sprite.textureIndex] & "," & $sprite.x & "," & $sprite.y & "," & $sprite.width & "," & $sprite.height & "," & $sprite.rotated & "\n"
