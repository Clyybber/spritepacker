type rect* = object
  x*, y*, width*, height*: int

template bottom*(r: rect): int = r.y
template `bottom=`*(r: var rect, val: int) = r.y = val
template top*(r: rect): int = r.y + r.height
template left*(r: rect): int = r.x
template `left=`*(r: var rect, val: int) = r.x = val
template right*(r: rect): int = r.x + r.width
template area*(r: rect): int = r.width * r.height
template intersects*(freeRect, usedRect: rect): bool =
  not(usedRect.left >= freeRect.right or
      usedRect.right <= freeRect.left or
      usedRect.bottom >= freeRect.top or
      usedRect.top <= freeRect.bottom)
template contains*(this, other: rect): bool =
  this.left <= other.left and this.right >= other.right and this.bottom <= other.bottom and this.top >= other.top

func totalUsedRect*(usedRects: seq[rect]): rect =
  assert(usedRects.len != 0)
  var
    left = int.high
    bottom = int.high
    width = 0
    height = 0
  for rect in usedRects:
    left = min(left, rect.left)
    bottom = min(bottom, rect.bottom)
    width = max(width, rect.right)
    height = max(height, rect.top)
  rect(x: left, y: bottom, width: width, height: height)

func splitFreeRect*(freeRect, usedRect: rect): seq[rect] =
  if usedRect.left < freeRect.right and usedRect.right > freeRect.left:
    if usedRect.bottom > freeRect.bottom and usedRect.bottom < freeRect.top: #new rect at the bottom side of the used rect
      var newFreeRect = freeRect
      newFreeRect.height = usedRect.bottom - freeRect.bottom
      result.add newFreeRect
    if usedRect.top < freeRect.top: #new rect at the top side of the used rect
      var newFreeRect = freeRect
      newFreeRect.bottom = usedRect.top
      newFreeRect.height = freeRect.top - usedRect.top
      result.add newFreeRect
  if usedRect.bottom < freeRect.top and usedRect.top > freeRect.bottom:
    if usedRect.left > freeRect.left and usedRect.left < freeRect.right: #new rect at the left side of the used rect
      var newFreeRect = freeRect
      newFreeRect.width = usedRect.left - freeRect.left
      result.add newFreeRect
    if usedRect.right < freeRect.right: #new rect at the right side of the used rect
      var newFreeRect = freeRect
      newFreeRect.left = usedRect.right
      newFreeRect.width = freeRect.right - usedRect.right
      result.add newFreeRect

func pruneFreeList*(freeRects: var seq[rect]) = #Go through each pair and remove any rectangle that is redundant.
  var i = 0
  while i < freeRects.len:
    var j = i + 1
    while j < freeRects.len:
      if freeRects[j].contains(freeRects[i]):
        freeRects.delete(i)
        dec i
        break
      elif freeRects[i].contains(freeRects[j]):
        freeRects.delete(j)
        dec j
      inc j
    inc i

func placeRect*(freeRects, usedRects: var seq[rect], newRect: rect) =
  var numRectanglesToProcess = freeRects.len
  var i = 0
  while i < numRectanglesToProcess:
    if intersects(freeRects[i], newRect):
      freeRects.add(splitFreeRect(freeRects[i], newRect))
      freeRects.delete(i) #this pops, so we have to decrement i and numRectangl.. afterwards
      dec i
      dec numRectanglesToProcess
    inc i
  freeRects.pruneFreeList()
  usedRects.add(newRect)
