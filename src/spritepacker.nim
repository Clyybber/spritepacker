import stb_image/read as stbi
import stb_image/write as stbiw
import packMethods
import rect
from math import sqrt

type sprite* = object
  width*, height*: int
  x*, y*: int
  rotated*: bool
  textureIndex*: int

import saveMethods

func pack(sprites: seq[sprite], totalWidth, totalHeight: int, allowRotation: bool, packMethod: packMethod): (seq[sprite], rect) =
  var unpacked: seq[sprite] = sprites
  var packed: seq[sprite]
  var freeRects: seq[rect] = @[rect(x: 0, y: 0, width: totalWidth, height: totalHeight)]
  var usedRects: seq[rect]
  while unpacked.len > 0:
    var primary = float.high
    var tieBreaker = int.high
    var bestRect: rect
    var bestRectIndex = -1
    for i, v in unpacked:
      let score = case packMethod
        of totalArea: totalAreaFit(v.width, v.height, freeRects, usedRects, allowRotation)
        of shortSide: shortSideFit(v.width, v.height, freeRects, usedRects, allowRotation)
        of longSide: longSideFit(v.width, v.height, freeRects, usedRects, allowRotation)
        of smallArea: smallAreaFit(v.width, v.height, freeRects, usedRects, allowRotation)
        of bottomLeft: bottomLeftFit(v.width, v.height, freeRects, usedRects, allowRotation)
        of contactPoint: contactPointFit(v.width, v.height, freeRects, usedRects, allowRotation, totalWidth, totalHeight)

      if score.rect.height == 0 or score.rect.width == 0:
        continue

      if score.primary < primary or (score.primary == primary and score.tieBreaker < tieBreaker):
        primary = score.primary
        tieBreaker = score.tieBreaker
        bestRect = score.rect
        bestRectIndex = i

    if bestRectIndex == -1: #About to fail...
      break

    placeRect(freeRects, usedRects, bestRect)
    var currSprite = unpacked[bestRectIndex]
    unpacked.delete(bestRectIndex)

    currSprite.x = bestRect.left
    currSprite.y = bestRect.bottom
    currSprite.rotated = bestRect.width != currSprite.width #rotated
    packed.add(currSprite)

  return (packed, totalUsedRect(usedRects))

func nextPowerOfTwo(v: int): int = #Actually this is next or the same power of two
  result = 1
  while result < v:
    result *= 2
func prevPowerOfTwo(v: int): int = nextPowerOfTwo(v) div 2

#TODO: unhardcode this, also maybe add a bit of variation
func someHigherValue(v: int): int = int(v.float * 1.1)
func someLowerValue(v: int): int = int(v.float * 0.9)

proc spritepacker(maxWidth=0, maxHeight=0, rotationAllowed=false, powerOfTwo=false, square=false, packMethod=totalArea, saveMethod=nimTuple, inputImgs: seq[string], outputImg: string, outputInfo: string, preferredChannels=stbi.RGB, maxAttempts=100): int =
  #Load everything
  var textures: seq[seq[byte]]
  var sprites: seq[sprite]
  for index, imagePath in inputImgs:
    var width, height, channels: int
    textures.add(stbi.load(imagePath, width, height, channels, preferredChannels))
    assert(channels == preferredChannels, imagePath & ": expected: " & $preferredChannels & " got: " & $channels)
    sprites.add(sprite(width: width, height: height, textureIndex: index))

  var res: seq[sprite]
  var minDimensions: rect
  var width = maxWidth
  var height = maxHeight

  #Maximum stuff
  var totalArea = 0
  for sprite in sprites:
    totalArea += sprite.width * sprite.height

  if maxWidth != 0 and maxHeight != 0:
    echo "Warning: Using maxWidth and maxHeight together might result in failure"
  if maxWidth != 0:
    height = totalArea div maxWidth
  if maxHeight != 0:
    width = totalArea div maxHeight
  if maxWidth == 0 and maxHeight == 0: #We are free to choose, yay!
    width  = sqrt(totalArea.float).int
    height = sqrt(totalArea.float).int

  #Try to pack as tight as we can until we fail. Then go one step back and thats the one.
  var lastRunFailed = false
  for _ in 0..<maxAttempts:

    (res, minDimensions) = pack(sprites, width, height, rotationAllowed, packMethod)
    echo "Occupancy without adhering to anything: ", totalArea / minDimensions.area
    echo "Occupancy with  adhering to the limits: ", totalArea / (width * height)

    if res.len < sprites.len: #Failed:
      if powerOfTwo:
        width  = nextPowerOfTwo(width + 1) #Plus one here, so we really get the *next* power of two,
        height = nextPowerOfTwo(height + 1)#and not the same power of two as we put in
      else:
        width  = someHigherValue(width)
        height = someHigherValue(height)
      if maxWidth != 0:
        width = min(width, maxWidth)
      if maxHeight != 0:
        height = min(height, maxHeight)
      if square:
        width  = min(width, height)
        height = min(width, height)
      lastRunFailed = true
      echo "failed"
    elif not lastRunFailed: #Still going
      if powerOfTwo:
        width  = prevPowerOfTwo(minDimensions.width) #Could also use width instead of minDimensions.width here,
        height = prevPowerOfTwo(minDimensions.height)#but minDimensions.width is probably slightly smaller...
      else:
        width  = someLowerValue(width) #Without constraints this step could be skipped (perhaps), so that we
        height = someLowerValue(height)#would do width = minDimensions.widht here, but then we would never fail...
      if square:
        width  = min(width, height)
        height = min(width, height)
      lastRunFailed = false
    else: #Last one failed, so we choose this successful one
      if powerOfTwo:
        width  = nextPowerOfTwo(minDimensions.width)
        height = nextPowerOfTwo(minDimensions.height)
      else:
        width  = minDimensions.width
        height = minDimensions.height
      if maxWidth != 0:
        width = min(width, maxWidth)
      if maxHeight != 0:
        height = min(height, maxHeight)
      if square:
        width  = max(width, height)
        height = max(width, height)
      lastRunFailed = false
      break
  if lastRunFailed:
    quit "Too many failed attempts, try increasing the maximum parameters"

  echo "Final occupancy: ", totalArea / (width * height)
  echo "Final dimensions: " ,width, "x", height

  #Save packing info
  let file = open(outputInfo, fmWrite)
  let packInfo = case saveMethod
    of nimTuple: nimTupleSav(inputImgs, res)
    of commaSepValues: commaSepValuesSav(inputImgs, res)
  file.write(packInfo)
  file.close

  #Save the texture
  var packedTexture = newSeq[byte](width * height * preferredChannels)
  for sprite in res:
    for x in 0..<sprite.width:
      for y in 0..<sprite.height:
        let outputCoord = if sprite.rotated: ((sprite.y+x) * width + sprite.x+y) * preferredChannels
                          else:              ((sprite.y+y) * width + sprite.x+x) * preferredChannels
        let inputCoord = (y * sprite.width + x) * preferredChannels
        for c in 0..<preferredChannels:
          packedTexture[outputCoord + c] = textures[sprite.textureIndex][inputCoord + c]
  writePNG(outputImg, width, height, preferredChannels, packedTexture)

  result = 0

when isMainModule:
  import cligen; dispatch(spritepacker)
