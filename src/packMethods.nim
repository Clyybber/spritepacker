import rect

type
  scoreResult* = tuple
    rect: rect
    primary: float
    tieBreaker: int
  packMethod* = enum
    totalArea #The default
    shortSide
    longSide
    smallArea
    bottomLeft
    contactPoint

func totalAreaFit*(width, height: int, freeRects, usedRects: seq[rect], allowRotation: bool): scoreResult =
  var bestAreaRatio = float.high
  var bestShortSide = int.high
  var bestRect: rect

  for rect in freeRects:
    if rect.width >= width and rect.height >= height: #Try to place the rectangle non-rotated
      let tempRect = rect(x: rect.x, y: rect.y, width: width, height: height)
      let totalRect = totalUsedRect(usedRects & tempRect)
      let areaRatio = totalRect.area / tempRect.area #Weight heuristic to lay larger down first
      let shortSide = min(totalRect.width, totalRect.height)
      if areaRatio < bestAreaRatio or (areaRatio == bestAreaRatio and shortSide < bestShortSide):
        bestRect = tempRect
        bestAreaRatio = areaRatio
        bestShortSide = shortSide

    if allowRotation and rect.width >= height and rect.height >= width: #Try to place it rotated
      let tempRect = rect(x: rect.x, y: rect.y, width: height, height: width) #Flipped width and height here
      let totalRect = totalUsedRect(usedRects & tempRect)
      let areaRatio = totalRect.area / tempRect.area #Weight heuristic to lay larger down first
      let shortSide = min(totalRect.width, totalRect.height)
      if areaRatio < bestAreaRatio or (areaRatio == bestAreaRatio and shortSide < bestShortSide):
        bestRect = tempRect
        bestAreaRatio = areaRatio
        bestShortSide = shortSide

  (rect: bestRect, primary: bestAreaRatio, tieBreaker: bestShortSide)


func shortSideFit*(width, height: int, freeRects, usedRects: seq[rect], allowRotation: bool): scoreResult =
  var bestShortSide = int.high
  var bestLongSide = int.high
  var bestRect: rect

  for rect in freeRects:
    #Try to place the rectangle in upright (non-flipped) orientation.
    if rect.width >= width and rect.height >= height:
      let leftoverHoriz = (rect.width - width).abs.int
      let leftoverVert = (rect.height - height).abs.int
      let shortSide = min(leftoverHoriz, leftoverVert)
      let longSide = max(leftoverHoriz, leftoverVert)
      if shortSide < bestShortSide or (shortSide == bestShortSide and longSide < bestLongSide):
        bestRect = rect(x: rect.x, y: rect.y, width: width, height: height)
        bestShortSide = shortSide
        bestLongSide = longSide

    if allowRotation and rect.width >= height and rect.height >= width:
      let flippedLeftoverHoriz = (rect.width - height).abs.int
      let flippedLeftoverVert = (rect.height - width).abs.int
      let flippedShortSide = min(flippedLeftoverHoriz, flippedLeftoverVert)
      let flippedLongSide = max(flippedLeftoverHoriz, flippedLeftoverVert)
      if flippedShortSide < bestShortSide or (flippedShortSide == bestShortSide and flippedLongSide < bestLongSide):
        bestRect = rect(x: rect.x, y: rect.y, width: height, height: width)
        bestShortSide = flippedShortSide
        bestLongSide = flippedLongSide

  (rect: bestRect, primary: bestShortSide.float, tieBreaker: bestLongSide)


func longSideFit*(width, height: int, freeRects, usedRects: seq[rect], allowRotation: bool): scoreResult =
  var bestShortSide = int.high
  var bestLongSide = int.high
  var bestRect: rect

  for rect in freeRects:
    #Try to place the rectangle in upright (non-flipped) orientation.
    if rect.width >= width and rect.height >= height:
      let leftoverHoriz = (rect.width - width).abs.int
      let leftoverVert = (rect.height - height).abs.int
      let shortSide = min(leftoverHoriz, leftoverVert)
      let longSide = max(leftoverHoriz, leftoverVert)

      if longSide < bestLongSide or (longSide == bestLongSide and shortSide < bestShortSide):
        bestRect = rect(x: rect.x, y: rect.y, width: width, height: height)
        bestShortSide = shortSide
        bestLongSide = longSide

    if allowRotation and rect.width >= height and rect.height >= width:
      let flippedLeftoverHoriz = (rect.width - height).abs.int
      let flippedLeftoverVert = (rect.height - width).abs.int
      let flippedShortSide = min(flippedLeftoverHoriz, flippedLeftoverVert)
      let flippedLongSide = max(flippedLeftoverHoriz, flippedLeftoverVert)

      if flippedLongSide < bestLongSide or (flippedLongSide == bestLongSide and flippedShortSide < bestShortSide):
        bestRect = rect(x: rect.x, y: rect.y, width: height, height: width)
        bestShortSide = flippedShortSide
        bestLongSide = flippedLongSide

  (rect: bestRect, primary: bestLongSide.float, tieBreaker: bestShortSide)


func smallAreaFit*(width, height: int, freeRects, usedRects: seq[rect], allowRotation: bool): scoreResult =
  var bestArea = int.high
  var bestShortSide = int.high
  var bestRect: rect

  for rect in freeRects:
    let area = rect.width * rect.height - width * height
    #Try to place the rectangle in upright (non-flipped) orientation.
    if rect.width >= width and rect.height >= height:
      let leftoverHoriz = (rect.width - width).abs.int
      let leftoverVert = (rect.height - height).abs.int
      let shortSide = min(leftoverHoriz, leftoverVert)
      if area < bestArea or (area == bestArea and shortSide < bestShortSide):
        bestRect = rect(x: rect.x, y: rect.y, width: width, height: height)
        bestShortSide = shortSide
        bestArea= area

    if allowRotation and rect.width >= height and rect.height >= width:
      let flippedLeftoverHoriz = (rect.width - height).abs.int
      let flippedLeftoverVert = (rect.height - width).abs.int
      let flippedShortSide = min(flippedLeftoverHoriz, flippedLeftoverVert)
      if area < bestArea or (area == bestArea and flippedShortSide < bestShortSide):
        bestRect = rect(x: rect.x, y: rect.y, width: height, height: width)
        bestShortSide = flippedShortSide
        bestArea = area

  (rect: bestRect, primary: bestArea.float, tieBreaker: bestShortSide)


func bottomLeftFit*(width, height: int, freeRects, usedRects: seq[rect], allowRotation: bool): scoreResult =
  var bestY = int.high
  var bestX = int.high
  var bestRect: rect

  for rect in freeRects:
    #Try to place the rectangle in upright (non-flipped) orientation.
    if rect.width >= width and rect.height >= height:
      let topSideY = rect.top
      if topSideY < bestY or (topSideY == bestY and rect.right < bestX):
        bestRect = rect(x: rect.x, y: rect.y, width: width, height: height)
        bestY = topSideY
        bestX = rect.right

    if allowRotation and rect.width >= height and rect.height >= width:
      let topSideY = rect.bottom + width
      if topSideY < bestY or (topSideY == bestY and rect.right < bestX):
        bestRect = rect(x: rect.x, y: rect.y, width: height, height: width)
        bestY = topSideY
        bestX = rect.right

  (rect: bestRect, primary: bestY.float, tieBreaker: bestX)


func commonIntervalLength(i1start, i1end, i2start, i2end: int): int =
  if i1end < i2start or i2end < i1start:
    return 0
  return min(i1end, i2end) - max(i1start, i2start)

func contactPointScoreNode(usedRects: seq[rect], x, y, width, height, totalWidth, totalHeight: int): int =
  if x == 0 or x + width == totalWidth:
    result += height
  if y == 0 or y + height == totalHeight:
    result += width
  for rect in usedRects:
    if rect.left == x + width or rect.right == x:
      result += commonIntervalLength(rect.bottom, rect.top, y, y + height)
    if rect.bottom == y + height or rect.top == y:
      result += commonIntervalLength(rect.left, rect.right, x, x + width)

func contactPointFit*(width, height: int, freeRects, usedRects: seq[rect], allowRotation: bool, totalWidth, totalHeight: int): scoreResult =
  var bestContactScore = 0
  var bestRect: rect

  for rect in freeRects:
    #Try to place the rectangle in upright (non-flipped) orientation.
    if rect.width >= width and rect.height >= height:
      let score = contactPointScoreNode(usedRects, rect.left, rect.bottom, width, height, totalWidth, totalHeight)
      if score > bestContactScore:
        bestRect = rect(x: rect.x, y: rect.y, width: width, height: height)
        bestContactScore = score

    if allowRotation and rect.width >= height and rect.height >= width:
      let score = contactPointScoreNode(usedRects, rect.left, rect.bottom, height, width, totalWidth, totalHeight)
      if score > bestContactScore:
        bestRect = rect(x: rect.x, y: rect.y, width: height, height: width)
        bestContactScore = score

  (rect: bestRect, primary: float(- bestContactScore), tieBreaker: 0)
